"use strict";

const config = require('config');

const DailyRotateFile = require('winston-daily-rotate-file');

let winston = require( 'winston' ),
    fs = require( 'fs' ),
    logDir = config.get('logDir'),
    env = process.env.NODE_ENV || 'development',
    logger;

winston.setLevels( winston.config.npm.levels );
winston.addColors( winston.config.npm.colors );

if ( !fs.existsSync( logDir ) ) {
    // Create the directory if it does not exist
    fs.mkdirSync( logDir );
}

logger = new( winston.Logger )( {
    transports: [
        new winston.transports.Console( {
            level: env === 'development' ? 'debug' : 'warn',
            colorize: true
        } ),
        new DailyRotateFile( {
            level: env === 'development' ? 'debug' : 'info',
            datePattern: '.yyyy-MM-dd',
            filename: logDir + '/solair.log',
            prettyPrint: true,
            json: false
        } )
    ],
    exceptionHandlers: [
        new DailyRotateFile( {
            filename: logDir + '/solair-exceptions.log',
            humanReadableUnhandledException: true,
            prettyPrint: true,
            json: false,
            datePattern: '.yyyy-MM-dd'
        } )
    ]
} );

module.exports = logger;