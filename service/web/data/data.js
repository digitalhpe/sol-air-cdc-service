"use strict";

const mongoose = require('mongoose');
const config = require('config');
const uuid = require('node-uuid');
const NodeCache = require( "node-cache" );
const log = require('./logger');

const Location = require('../model/locationSchema');
const State = require('../model/state');

const data = module.exports = {};
const cache = new NodeCache();
const HOUR_CACHE = 3600;
const STATE_CACHE_NAME = "states";
const NoneFound = "None Found";

mongoose.connect(config.get('connectionString'));

data.saveLocation = function(location, callback) {

    if(location.locationid === "0") {
        location.locationid = uuid.v4();
        save(location);
    } else {
        this.findLocation(location.locationid, function(err, data){
            if(err) {
                callback(err, null);
            } else {
                data.name = location.name;
                data.favorite = location.favorite;
                data.notify = location.notify;
                data.os = location.os;
                save(data);
            }
        });
    }

    function save(loc) {
        loc.save(function (err) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, loc.locationid);
            }
        });
    }
};

data.getLocations = function(id, callback) {

    Location.find({pushid: id}, function(err, locations) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, locations);
        }
    });
};

data.findLocation = function(id, callback) {

    Location.find({locationid: id}, function(err, locations){
        if(err) {
            callback(err, null);
        } else {
            callback(null, locations[0]);
        }
    });
};

data.getPushLocations = function(platform, callback) {

    Location.find({notify: true, os: platform}, function(err, locations) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, locations);
        }
    });
};

data.deleteLocation = function(locationid, callback) {

    Location.remove({"locationid" : locationid }, function (error) {
        callback(error);
    });
};

data.getStates = async function() {
    return new Promise(function (resolve, reject) {

        let states = cache.get(STATE_CACHE_NAME);
        if ( states !== undefined ){
            log.debug('states cache hit');
            resolve(states);
        } else {
            log.debug('states cache miss');

            const conditions = null;
            const projection = null;
            const options = null;

            State.find(conditions, projection, options, function (error, data) {
                if (error) {
                    reject(error);
                } else if (data === null || data.length === 0) {
                    const result = {};
                    result.message = NoneFound;
                    reject(result);
                } else {
                    let success = cache.set( STATE_CACHE_NAME, data, HOUR_CACHE/4 );
                    if(!success) {
                        reject("states cache fail");
                    }
                    resolve(data);
                }
            });
        }
    });
};