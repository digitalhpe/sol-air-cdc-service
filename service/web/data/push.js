"use strict";

const apn = require('apn');
const gcm = require('node-gcm');
const async = require('async');
const config = require('config');
const logger = require('./logger');

const push = module.exports = {};

push.sendNotifications = function(db, epaClient) {
    logger.info('Starting push notifications');

    async.parallel({
        ios: function (callback) {
            db.getPushLocations("ios", function(err, data){
                callback(err, data);
            });
        },
        android: function (callback) {
            db.getPushLocations("android", function(err, data){
                callback(err, data);
            });
        }
    }, function (err, results){
        if(!err) {

            iosPush(results.ios);

            androidPush(results.android);
            logger.info('Finished push notifications');
        }
    });

    function iosPush(records) {
        if(records.length > 0) {

            const service = startApnsService();

            epaClient.getCurrent(records, function(err, data) {

                if(err) {
                    logger.error(err);
                } else {
                    for(let i = 0; i < data.length; i++) {

                        if(data[i].uvalert) {
                            sendApns(data[i].pushid, "UV Alert for " + data[i].name, service);
                        }

                        if(records[i].aqialert) {
                            sendApns(data[i].pushid, "Air Quality Alert for " + data[i].name, service);
                        }
                    }

                    service.shutdown();
                }
            });
        }
    }

    function startApnsService() {
        const service = new apn.connection({ production: false, passphrase: "solavi_epa", cert: "config/cert.pem", key: "config/key.pem" });

        service.on("connected", function() {
             logger.debug("Connected to APNS");
        });

        service.on("transmitted", function(notification, device) {
            logger.info("APNS Notification transmitted to: " + device.token.toString("hex"));
        });

        service.on("transmissionError", function(errCode, notification, device) {
            logger.error("APNS Notification caused error: " + errCode + " for device ", device, notification);
            if (errCode === 8) {
                logger.debug("A error code of 8 indicates that the device token is invalid. This could be for a number of reasons - are you using the correct environment? i.e. Production vs. Sandbox");
            }
        });

        service.on("timeout", function () {
            logger.error("APNS Connection Timeout");
        });

        service.on("disconnected", function() {
            logger.debug("APNS Disconnected");
        });

        service.on("socketError", console.error);

        return service;
    }

    function sendApns(token, message, service) {
        const note = new apn.notification();
        note.setAlertText(message);
        note.badge = 1;
        note.sound = "default";
        note.category = "SOLAVI";
        service.pushNotification(note, token);
    }

    function androidPush(records) {
        if(records.length > 0) {

            const key = config.get('gcmKey');
            const sender = new gcm.Sender(key);

            epaClient.getCurrent(records, function(err, data) {
                if(err) {
                    logger.error(err);
                } else {
                    for(let i = 0; i < data.length; i++) {
                        if(data[i].uvalert) {
                            sendGcm(data[i].pushid, "UV Alert for " + data[i].name, sender);
                        }

                        if(data[i].aqialert) {
                            sendGcm(data[i].pushid, "Air Quality Alert for " + data[i].name, sender);
                        }
                    }
                }
            });
        }
    }

    function sendGcm(token, body, sender) {
        const message = new gcm.Message();
        message.addNotification('title', 'Sol•Avi');
        message.addNotification('icon', 'ic_launcher');
        message.addNotification('body', body);

        const tokens = [];
        tokens.push(token);
        sender.send(message, { registrationTokens: tokens }, function (err, response) {
            if(err) {
                logger.error(err);
            }
            else {
                logger.info("GCM Notification sent to: " + token);
            }
        });
    }
};