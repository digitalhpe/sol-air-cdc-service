"use strict";

/**
 * Created by trux on 12/17/15.
 */

const parser = module.exports = {};


parser.parseAqi = function(raw) {
    const aqi = {};
    for (let i = 0; i < raw.length; i++) {
        if (raw[i].ParameterName === 'O3') {
            aqi.o3 = raw[i].AQI;
        } else if (raw[i].ParameterName === 'OZONE') {
            aqi.o3 = raw[i].AQI;
        } else if(raw[i].ParameterName === 'PM2.5') {
            aqi.pm25 = raw[i].AQI;
        } else if(raw[i].ParameterName === 'PM10') {
            aqi.pm10 = raw[i].AQI;
        }
    }

    if(aqi.o3 === undefined) {
        aqi.o3 = -1;
    }

    if(aqi.pm25 === undefined) {
        aqi.pm25 = -1;
    }

    if(aqi.pm10 === undefined) {
        aqi.pm10 = -1;
    }

    return aqi;
};



parser.parseDetailResults = function(input, data) {

    if(input.locationid === "0") {
        input.favorite = false;
        input.notify = false;
        input.name = "";
        input.latitude = "";
        input.longitude = "";
        input.os = "";
    }

    if (data.rawUv.length > 0) {
        input.uv = data.rawUv[0].UV_INDEX;
    }

    if(data.rawAqi.length > 0) {
        input.aqi = this.parseAqi(data.rawAqi);
    }

    input.history = {};


    if(data.rawHistoryPlus.length > 0) {
        input.history.aqi = this.parseAqi(data.rawHistoryAqi);
    }

    if(data.rawHistoryPlus.length > 0) {
        input.history.aqiplus = this.parseAqi(data.rawHistoryPlus);
    }

    if(data.rawHistoryMinus.length > 0) {
        input.history.aqiminus = this.parseAqi(data.rawHistoryMinus);
    }

    return input;
};