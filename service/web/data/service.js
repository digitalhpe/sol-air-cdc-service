"use strict";

const async = require('async');
const request = require('request');
const config = require('config');
const logger = require('./logger');

const moment = require('moment');
moment().format();

const PERCENT_FLOODPLAIN = 582;
const PERCENT_NEARPARK = 428;
const PERCENT_ATHSMA_HOSP = 101;
const PERCENT_ATHSMA_ER = 436;

const service = module.exports = {};


service.getHistory = function(zip, epoch, masterCallback) {

    const key = config.get('epaKey');

    const day = moment.unix(epoch);
    const dayMinus = moment.unix(epoch).subtract(7, "d");
    const dayPlus = moment.unix(epoch).add(7, "d");

    async.parallel({

        historyAqi: function(callback) {
            getHistoricalData(zip, day, key, callback);
        },
        historyPlus: function(callback) {
            getHistoricalData(zip, dayPlus, key, callback);
        },
        historyMinus: function(callback) {
            getHistoricalData(zip, dayMinus, key, callback);
        }

    }, function (err, results) {

        const serviceResults = {};
        serviceResults.rawHistoryAqi = results.historyAqi;
        serviceResults.rawHistoryPlus = results.historyPlus;
        serviceResults.rawHistoryMinus = results.historyMinus;

        masterCallback(null, serviceResults);
    });
};


service.getDetail = function(location, detailCallback) {

    const key = config.get('epaKey');

    function errorResult() {
        const data = {};
        data.index = 0;
        data.alert = false;
        return data;
    }

    const current = moment();

    const lastYear = moment().subtract(1, "y");
    const lastYearMinus = moment().subtract(1, "y").subtract(7, "d");
    const lastYearPlus = moment().subtract(1, "y").add(7, "d");

    async.parallel({
        uv: function (callback) {
            const url = 'http://iaspub.epa.gov/enviro/efservice/getEnvirofactsUVDAILY/ZIP/' + location.zip + '/JSON';

            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    const result = JSON.parse(body);
                    callback(null, result);
                } else {
                    callback(null, errorResult());
                }
            });
        },
        aqi: function (callback) {
            const url = 'http://www.airnowapi.org/aq/observation/zipCode/current/?format=application/json&zipCode=' + location.zip + '&distance=25&API_KEY=' + key;

            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {

                    const result = JSON.parse(body);
                    logger.debug(body);
                    if(result.length > 0) {
                        callback(null, result);
                    } else {
                        callback(null, errorResult());
                    }
                } else {
                    callback(null, errorResult());
                }
            });
        },
        historyAqi: function(callback) {
            getHistoricalData(location.zip, lastYear, key, callback);
        },
        historyPlus: function(callback) {
            getHistoricalData(location.zip, lastYearPlus, key, callback);
        },
        historyMinus: function(callback) {
            getHistoricalData(location.zip, lastYearMinus, key, callback);
        }

    }, function (err, results) {

        const serviceResults = {};
        serviceResults.rawUv = results.uv;
        serviceResults.rawAqi = results.aqi;
        serviceResults.rawHistoryAqi = results.historyAqi;
        serviceResults.rawHistoryPlus = results.historyPlus;
        serviceResults.rawHistoryMinus = results.historyMinus;

        detailCallback(null, serviceResults);
    });
};



function getHistoricalData(zipcode, time, key, callback) {

    function errorResult() {
        const data = {};
        data.ozone = 0;
        data.pm25 = 0;
        data.pm10 = 0;
        return data;
    }

    const url = "http://www.airnowapi.org/aq/observation/zipCode/historical/?format=application/json&zipCode=" + zipcode + "&date=" + time.format("YYYY-MM-DD") + "T00-0000&distance=25&API_KEY=" + key;

    request(url, function (error, response, body) {
        if (!error && response.statusCode === 200) {

            const result = JSON.parse(body);
            //console.log(result);
            if(result.length > 0) {
                callback(null, result);
            } else {
                const raw = [];

                const o3 = {};
                o3.ParameterName = "O3";
                o3.AQI = -1;
                raw.push(o3);

                const pm10 = {};
                pm10.ParameterName = "PM10";
                pm10.AQI = -1;
                raw.push(pm10);

                const pm25 = {};
                pm25.ParameterName = "PM2.5";
                pm25.AQI = -1;
                raw.push(pm25);

                callback(null, raw);
            }
        } else {
            callback(null, errorResult());
        }
    });
}

service.getCurrent = function(locations, masterCallback) {

    async.map(locations, function(document, next){

        getCurrentData(document, function(err, data){
           if(err) {
               console.log(err);
           } else {
               document.uv = data.uv;
               document.uvalert = data.uvalert;
               document.ozone = data.ozone;
               document.aqialert = data.aqialert;

               next(null, document);
           }
        });

    }, function(err, results) {
        if(err) {
            logger.error(err);
        } else {
            masterCallback(null, results);
        }
    });

};

service.getLanding = function(locations, masterCallback) {

    let count = 0;
    const locationList = [];
    async.whilst (
        function () {
            return count < locations.length;
        },
        function(callback) {
            count++;
            getCurrentData(locations[count-1], function(err, location){
                locationList.push(location);
                callback(err, locationList);
            });
        },
        function(err, data) {
            masterCallback(err, locationList);
        }
    );
};

function getCurrentData(location, locationCallback) {

    const key = config.get('epaKey');

    function errorResult() {
        const data = {};
        data.index = 0;
        data.alert = false;
        return data;
    }

    async.parallel({
        uv: function (callback) {
            const url = 'http://iaspub.epa.gov/enviro/efservice/getEnvirofactsUVDAILY/ZIP/' + location.zip + '/JSON';

            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    const result = JSON.parse(body);
                    let uv = {};
                    if (result.length > 0) {
                        uv.index = result[0].UV_INDEX;
                        uv.alert = result[0].UV_INDEX >= 3;
                    } else {
                        uv = errorResult();
                    }
                    callback(null, uv);
                } else {
                    callback(null, errorResult());
                }
            });
        },
        aqi: function (callback) {
            const url = 'http://www.airnowapi.org/aq/observation/zipCode/current/?format=application/json&zipCode=' + location.zip + '&distance=25&API_KEY=' + key;
            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    const result = JSON.parse(body);
                    const data = {};
                    if(result.length > 0) {
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].ParameterName === 'O3') {
                                data.index = result[i].AQI;
                                data.alert = result[i].AQI >= 51;
                                callback(null, data);
                                break;
                            }
                        }
                        if(data.index === undefined) {
                            callback(null, errorResult());
                        }
                    } else {
                        callback(null, errorResult());
                    }
                } else {
                    callback(null, errorResult());
                }
            });
        }
    }, function (err, results) {
        const newLocation = {};
        newLocation.uv = results.uv.index;
        newLocation.uvalert = results.uv.alert;
        newLocation.ozone = results.aqi.index;
        newLocation.aqialert = results.aqi.alert;
        newLocation.locationid = location.locationid;
        newLocation.zip = location.zip;
        newLocation.name = location.name;
        newLocation.latitude = location.latitude;
        newLocation.longitude = location.longitude;
        newLocation.favorite = location.favorite;
        newLocation.park = location.park;
        newLocation.flood = location.flood;
        newLocation.measures = location.measures;
        newLocation.county = location.county;
        newLocation.state = location.state;

        locationCallback(null, newLocation);
    });
}

function getMeasure(year, stateCode, measureId) {

    const url = `https://ephtracking.cdc.gov/apigateway/api/v1/getData/${measureId}/${stateCode}/1/${year}/0/json?apiToken=${config.get("cdcKey")}`;

    return new Promise(function (resolve, reject) {
        request(url, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                const result = JSON.parse(body);
                result.measure = measureId;
                resolve(result);
            } else {
                reject(error);
            }
        });
    });
}

service.getAllMeasures = function (state, county) {

    return new Promise(function (resolve, reject) {

        Promise.all([
            getMeasure(2011, state, PERCENT_FLOODPLAIN),
            getMeasure(2010, state, PERCENT_NEARPARK),
            getMeasure(2010, state, PERCENT_ATHSMA_HOSP),
            getMeasure(2011, state, PERCENT_ATHSMA_HOSP),
            getMeasure(2012, state, PERCENT_ATHSMA_HOSP),
            getMeasure(2013, state, PERCENT_ATHSMA_HOSP),
            getMeasure(2014, state, PERCENT_ATHSMA_HOSP),
            getMeasure(2010, state, PERCENT_ATHSMA_ER),
            getMeasure(2011, state, PERCENT_ATHSMA_ER),
            getMeasure(2012, state, PERCENT_ATHSMA_ER),
            getMeasure(2013, state, PERCENT_ATHSMA_ER),
            getMeasure(2014, state, PERCENT_ATHSMA_ER)]
        ).then(function (resultList) {
            let result = {};
            result.state = state;
            result.measures = [];
            for (let i = 0; i < resultList.length; i++) {
                switch (resultList[i].measure) {
                    case PERCENT_FLOODPLAIN:
                        result.flood = processCountyMeasure(county, resultList[i]);
                        break;
                    case PERCENT_NEARPARK:
                        result.park = processCountyMeasure(county, resultList[i]);
                        break;
                    case PERCENT_ATHSMA_ER:
                    case PERCENT_ATHSMA_HOSP:
                        const foundCounty = processResult(county, resultList[i], result.measures);
                        if (foundCounty !== "") {
                            result.county = foundCounty;
                        }
                        break;
                }
            }

            resolve(result);
        }).catch(error => {
            reject(error);
        });
    });
};

function processCountyMeasure(county, data) {
    let result = "";
    for(let j = 0; j < data.tableResult.length; j++) {
        if (data.tableResult[j].geo === county) {
            result = data.tableResult[j].dataValue;
        }
    }
    return result;
}

function processResult(county, data, measures){
    const result = {};
    let countyName = "";

    for(let j = 0; j < data.tableResult.length; j++) {
        if(data.tableResult[j].geo === county){
            if(measures.length > 0 ) {
                let found = false;
                for(let k = 0; k < measures.length; k++) {
                    if (measures[k].year === data.tableResult[j].year) {
                        if (data.measure === 101) {
                            measures[k].hospital = data.tableResult[j].dataValue;
                        } else {
                            measures[k].er = data.tableResult[j].dataValue;
                        }
                        countyName = data.tableResult[j].geo;
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    result.year = data.tableResult[j].year;
                    if(data.measure === 101) {
                        result.hospital = data.tableResult[j].dataValue;
                    } else {
                        result.er = data.tableResult[j].dataValue;
                    }
                    measures.push(result);
                    countyName = data.tableResult[j].geo;
                }

            } else {
                result.year = data.tableResult[j].year;
                if(data.measure === 101) {
                    result.hospital = data.tableResult[j].dataValue;
                } else {
                    result.er = data.tableResult[j].dataValue;
                }
                measures.push(result);
                countyName = data.tableResult[j].geo;
            }

        }
    }

    return countyName;
}