"use strict";

const forever = require('forever-monitor');

const child = new(forever.Monitor)('start/www', {
    max: 10,
    silent: true,
    options: []
});

child.on('exit', function() {
    console.log('app.js has exited after 10 restarts');
});

child.start();