"use strict";

const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');

const home = require('./routes/index');
const summary = require('./routes/summary');
const location = require('./routes/location');
const cdc = require('./routes/cdc');
const hello = require('./routes/hello');

const dummyPush = require('./routes/push');
const log = require('./data/logger');

const schedule = require('node-schedule');
const push = require('./data/push');

let testConfig = false;
if (process.env.NODE_ENV === 'test') {
  testConfig = true;
  log.info('Unit Test Mode Enabled');
} else {
    log.info('Normal Mode');
}



let dataLayer;
let serviceClient;

if (testConfig) {
  dataLayer = require('./test/mockDataLayer.js');
  serviceClient = require('./test/mockServiceLayer.js');
} else {
  dataLayer = require('./data/data.js');
  serviceClient = require('./data/service');
}
// Fill the states cache
dataLayer.getStates();


// Every Hour: 0 * * * *
// Every Minute: */1 * * * *
// Oncer per day at 10:45 AM: 45 10 * * *
const j = schedule.scheduleJob('45 10 * * *', function(){
    push.sendNotifications(dataLayer, serviceClient);
});


const utils = require('./start/common.js');

const app = express();
app.locals.utils = utils;
app.locals.dataLayer = dataLayer;
app.locals.service = serviceClient;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/push', dummyPush);
app.use('/summary', summary);
app.use('/location', location);
app.use('/cdc', cdc);
app.use('/hello', hello);
app.use('/', home);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
