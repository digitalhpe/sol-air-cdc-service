"use strict";

const service = module.exports = {};

service.getAllMeasures = function (state, county) {
    return new Promise(function (resolve, reject) {

        const result = {};

        result.state = state;
        result.county = county;
        result.park = 1.5;
        result.flood = 4.3;
        result.measures = [];

        const measure1 = {};
        measure1.year = 2012;
        measure1.hospital = 4.2;
        measure1.er = 5.6;
        result.measures.push(measure1);

        resolve(result);
    });
};

service.getHistory = function(zip, epoch, masterCallback) {

    const raw = [];

    const o3 = {};
    o3.ParameterName = "O3";
    o3.AQI = 100;
    raw.push(o3);

    const pm10 = {};
    pm10.ParameterName = "PM10";
    pm10.AQI = 78;
    raw.push(pm10);

    const pm25 = {};
    pm25.ParameterName = "PM2.5";
    pm25.AQI = 125;
    raw.push(pm25);

    const serviceResults = {};
    serviceResults.rawHistoryAqi = raw;
    serviceResults.rawHistoryPlus = raw;
    serviceResults.rawHistoryMinus = raw;

    masterCallback(null, serviceResults);
};


service.getDetail = function(location, detailCallback) {

    const serviceResults = {};

    const raw = [];

    const o3 = {};
    o3.ParameterName = "O3";
    o3.AQI = 100;
    raw.push(o3);

    const pm10 = {};
    pm10.ParameterName = "PM10";
    pm10.AQI = 78;
    raw.push(pm10);

    const pm25 = {};
    pm25.ParameterName = "PM2.5";
    pm25.AQI = 125;
    raw.push(pm25);

    const uvData = [];
    const uv = {};
    uv.UV_INDEX = 3;
    uvData.push(uv);


    serviceResults.rawUv = uvData;
    serviceResults.rawAqi = raw;
    serviceResults.rawHistoryAqi = raw;
    serviceResults.rawHistoryPlus = raw;
    serviceResults.rawHistoryMinus = raw;

    detailCallback(null, serviceResults);
};


service.getLanding = function(locations, masterCallback) {

    const result = [];

    for(let i = 0; i < locations.length; i++) {
        const newLocation = {};
        newLocation.uv = 2;
        newLocation.uvalert = false;
        newLocation.ozone = 100;
        newLocation.aqialert = false;
        newLocation.locationid = locations[i].locationid;
        newLocation.zip = locations[i].zip;
        newLocation.name = locations[i].name;
        newLocation.latitude = locations[i].latitude;
        newLocation.longitude = locations[i].longitude;
        newLocation.favorite = locations[i].favorite;
        result.push(newLocation);
    }

    masterCallback(null, result);

};

service.getMeasure = function(year, stateCode, measureId) {
    const PERCENT_FLOODPLAIN = 582;
    const PERCENT_NEARPARK = 428;
    const PERCENT_ATHSMA_HOSP = 101;
    const PERCENT_ATHSMA_ER = 436;

    switch (measureId) {
        case PERCENT_FLOODPLAIN:
            return {
                "legendResult": [],
                "tableResult": [
                    {
                        "id": "3240",
                        "dataValue": "1.62",
                        "displayValue": "1.62%",
                        "year": "2011",
                        "groupById": "1",
                        "geographicTypeId": 2,
                        "calculationType": "Percent",
                        "noDataId": -1,
                        "noDataBreakGroup": 0,
                        "stabilityFlag": "1",
                        "suppressionFlag": "0",
                        "title": "Apache, AZ",
                        "rollover": [
                            "Percent: 1.62%"
                        ],
                        "parentGeoId": "04",
                        "parentGeo": "Arizona",
                        "parentGeoAbbreviation": "AZ",
                        "geoAbbreviation": "04001",
                        "geo": "Apache",
                        "geoId": "04001"
                    }
                ],
                "tableResultWithCI": [],
                "devDisabilitiesTableResult": [],
                "modeledTableResult": [],
                "healthImpactTableResult": [],
                "climateChangeTableResult": [],
                "tableResultWithMonth": [],
                "tableResultWithQuarter": [],
                "dailyEstimatesTableResult": [],
                "heatEpisodesTableResult": [],
                "pwsTableResult": [],
                "biomonitoringTableResult": [],
                "benchmarkInformation": null,
                "benchmarkResult": [],
                "measureStratificationLevel": {
                    "id": 0,
                    "measureId": 0,
                    "stratificationLevelId": 0,
                    "smoothingTypeId": 0,
                    "suppressionTypeId": 0
                },
                "lookupList": {},
                "measureInformationDTO": {
                    "measureInformation": {
                        "calculationId": 0,
                        "calculation": null,
                        "calculationType": null,
                        "calculationColumnName": null,
                        "parentTemporalType": null,
                        "temporalType": null,
                        "temporalColumnName": null,
                        "temporalCount": 0,
                        "hasStability": null,
                        "smoothingType": null,
                        "precision": 0,
                        "hasConfidenceInterval": null,
                        "confidenceIntervalName": null,
                        "hasMultiMeasure": null,
                        "colorCount": 0,
                        "units": null
                    },
                    "tableDisplayDTO": [],
                    "chartDisplayDTO": [],
                    "mapDisplayDTO": [],
                    "chartAxisExceptionDTO": [],
                    "measureCalculationExceptionDTO": [],
                    "measureDisplayException": []
                },
                "tableResultClass": "TableResult",
                "tableReturnType": "tableResult",
                "publicAPIUrl": "/apigateway/api/v1/getCoreHolder/582/2/1/4/2011/0/0?apiToken=TOKEN&otherValue=-2fbdd84efb176723837f2fc0bd31b312f954f803a51850ad167b4e6264acfe013371b184f24f216d2b3a691c6ef32dcdddd72e15c0e37d04&timeStamp=1497884365533",
                "publicAPIServerUrl": "https://ephtracking.cdc.gov:443",
                "fullPublicAPIUrl": "https://ephtracking.cdc.gov:443/apigateway/api/v1/getCoreHolder/582/2/1/4/2011/0/0?apiToken=TOKEN&otherValue=-2fbdd84efb176723837f2fc0bd31b312f954f803a51850ad167b4e6264acfe013371b184f24f216d2b3a691c6ef32dcdddd72e15c0e37d04&timeStamp=1497884365533"
            };

        case PERCENT_NEARPARK:
            return {
                "legendResult": [],
                "tableResult": [
                    {
                        "id": "334840",
                        "dataValue": "4",
                        "displayValue": "4%",
                        "year": "2010",
                        "groupById": "1",
                        "geographicTypeId": 2,
                        "calculationType": "Percent",
                        "noDataId": -1,
                        "noDataBreakGroup": 0,
                        "stabilityFlag": "1",
                        "suppressionFlag": "0",
                        "title": "Apache, AZ",
                        "rollover": [
                            "Percent: 4%"
                        ],
                        "parentGeoId": "04",
                        "parentGeo": "Arizona",
                        "parentGeoAbbreviation": "AZ",
                        "geoAbbreviation": "04001",
                        "geo": "Apache",
                        "geoId": "04001"
                    }
                ],
                "tableResultWithCI": [],
                "devDisabilitiesTableResult": [],
                "modeledTableResult": [],
                "healthImpactTableResult": [],
                "climateChangeTableResult": [],
                "tableResultWithMonth": [],
                "tableResultWithQuarter": [],
                "dailyEstimatesTableResult": [],
                "heatEpisodesTableResult": [],
                "pwsTableResult": [],
                "biomonitoringTableResult": [],
                "benchmarkInformation": {
                    "id": 103,
                    "measureId": 428,
                    "benchmarkId": 14,
                    "measureGeographicTypeId": 2,
                    "units": null,
                    "geographicDisplay": "State",
                    "benchmarkName": " Percent",
                    "benchmarkShortName": " Percent",
                    "multipleSelectionActionId": 3,
                    "multipleSelectionAction": "Halt",
                    "active": null,
                    "hasMap": true,
                    "hasChart": false,
                    "hasTable": true,
                    "geographicTypeDisplay": "State Benchmark",
                    "benchmarkFullName": "State  Percent"
                },
                "benchmarkResult": [
                    {
                        "id": "12010",
                        "dataValue": "38",
                        "displayValue": "38%",
                        "year": "2010",
                        "groupById": "1",
                        "geographicTypeId": 2,
                        "calculationType": null,
                        "parentGeoId": "04",
                        "parentGeo": "Arizona",
                        "parentGeoAbbreviation": null,
                        "geoAbbreviation": null,
                        "geo": null,
                        "geoId": null
                    }
                ],
                "measureStratificationLevel": {
                    "id": 0,
                    "measureId": 0,
                    "stratificationLevelId": 0,
                    "smoothingTypeId": 0,
                    "suppressionTypeId": 0
                },
                "lookupList": {},
                "measureInformationDTO": {
                    "measureInformation": {
                        "calculationId": 0,
                        "calculation": null,
                        "calculationType": null,
                        "calculationColumnName": null,
                        "parentTemporalType": null,
                        "temporalType": null,
                        "temporalColumnName": null,
                        "temporalCount": 0,
                        "hasStability": null,
                        "smoothingType": null,
                        "precision": 0,
                        "hasConfidenceInterval": null,
                        "confidenceIntervalName": null,
                        "hasMultiMeasure": null,
                        "colorCount": 0,
                        "units": null
                    },
                    "tableDisplayDTO": [],
                    "chartDisplayDTO": [],
                    "mapDisplayDTO": [],
                    "chartAxisExceptionDTO": [],
                    "measureCalculationExceptionDTO": [],
                    "measureDisplayException": []
                },
                "tableResultClass": "TableResult",
                "tableReturnType": "tableResult",
                "publicAPIUrl": "/apigateway/api/v1/getCoreHolder/428/2/1/4/2010/0/0?apiToken=TOKEN&otherValue=7e5421a1d4be4de7f2a9fe5801063cbe06ab07fc5ae7af52e984b19d9b5301fecc8e4e7b0db0de92d4c596e3910cd2322228d1ea3f1c82fc&timeStamp=1497887025884",
                "publicAPIServerUrl": "https://ephtracking.cdc.gov:443",
                "fullPublicAPIUrl": "https://ephtracking.cdc.gov:443/apigateway/api/v1/getCoreHolder/428/2/1/4/2010/0/0?apiToken=TOKEN&otherValue=7e5421a1d4be4de7f2a9fe5801063cbe06ab07fc5ae7af52e984b19d9b5301fecc8e4e7b0db0de92d4c596e3910cd2322228d1ea3f1c82fc&timeStamp=1497887025884"
            };
        case PERCENT_ATHSMA_HOSP:
        case PERCENT_ATHSMA_ER:
            return {
                "legendResult": [],
                "tableResult": [
                    {
                        "id": "40927495",
                        "dataValue": "4.6",
                        "displayValue": "4.6",
                        "year": "2010",
                        "groupById": "1",
                        "geographicTypeId": 2,
                        "calculationType": "Crude Rate",
                        "noDataId": -1,
                        "noDataBreakGroup": 0,
                        "stabilityFlag": "1",
                        "suppressionFlag": "0",
                        "title": "Apache, AZ",
                        "rollover": [
                            "Crude Rate: 4.6"
                        ],
                        "parentGeoId": "04",
                        "parentGeo": "Arizona",
                        "parentGeoAbbreviation": "AZ",
                        "geoAbbreviation": "04001",
                        "geo": "Apache",
                        "geoId": "04001"
                    }
                ],
                "tableResultWithCI": [],
                "devDisabilitiesTableResult": [],
                "modeledTableResult": [],
                "healthImpactTableResult": [],
                "climateChangeTableResult": [],
                "tableResultWithMonth": [],
                "tableResultWithQuarter": [],
                "dailyEstimatesTableResult": [],
                "heatEpisodesTableResult": [],
                "pwsTableResult": [],
                "biomonitoringTableResult": [],
                "benchmarkInformation": null,
                "benchmarkResult": [],
                "measureStratificationLevel": {
                    "id": 0,
                    "measureId": 0,
                    "stratificationLevelId": 0,
                    "smoothingTypeId": 0,
                    "suppressionTypeId": 0
                },
                "lookupList": {},
                "measureInformationDTO": {
                    "measureInformation": {
                        "calculationId": 0,
                        "calculation": null,
                        "calculationType": null,
                        "calculationColumnName": null,
                        "parentTemporalType": null,
                        "temporalType": null,
                        "temporalColumnName": null,
                        "temporalCount": 0,
                        "hasStability": null,
                        "smoothingType": null,
                        "precision": 0,
                        "hasConfidenceInterval": null,
                        "confidenceIntervalName": null,
                        "hasMultiMeasure": null,
                        "colorCount": 0,
                        "units": null
                    },
                    "tableDisplayDTO": [],
                    "chartDisplayDTO": [],
                    "mapDisplayDTO": [],
                    "chartAxisExceptionDTO": [],
                    "measureCalculationExceptionDTO": [],
                    "measureDisplayException": []
                },
                "tableResultClass": "TableResult",
                "tableReturnType": "tableResult",
                "publicAPIUrl": "/apigateway/api/v1/getCoreHolder/101/2/1/4/2010/0/0?apiToken=TOKENA&otherValue=2c6d2ed0c83382987b299c664815141b06ab07fc5ae7af52e984b19d9b5301fecc8e4e7b0db0de92d4c596e3910cd2322228d1ea3f1c82fc&timeStamp=1497887126738",
                "publicAPIServerUrl": "https://ephtracking.cdc.gov:443",
                "fullPublicAPIUrl": "https://ephtracking.cdc.gov:443/apigateway/api/v1/getCoreHolder/101/2/1/4/2010/0/0?apiToken=TOKEN&otherValue=2c6d2ed0c83382987b299c664815141b06ab07fc5ae7af52e984b19d9b5301fecc8e4e7b0db0de92d4c596e3910cd2322228d1ea3f1c82fc&timeStamp=1497887126738"
            };
    }
};