"use strict";

const should = require('should');
const assert = require('assert');
const request = require('supertest');
const logger = require('../data/logger');
const app = require('../app');

const port = 8000;

describe('Index Page', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .get('/')
            .end(function (err, res) {
                if (err) {
                    logger.error(err);
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});