"use strict";

const should = require('should');
const assert = require('assert');
const request = require('supertest');
const app = require('../app');

describe('Adding a location', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .post('/location/add/')
            .send({
                "pushid":"111",
                "latitude":"42.329135",
                "longitude":"-83.045981",
                "zip":"48226",
                "favorite": false,
                "name":"Guardian Building" ,
                "notify": false,
                "os": "android",
                "locationid": 0,
                "state" : "4",
                "county" : "Apache"
            })
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});

describe('Getting a location\'s AQI history', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .get('/location/history/48154/1450799828')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});

describe('Getting an unknown location\'s Detail', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .get('/location/0/48154')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});

describe('Getting a known location\'s Detail', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .get('/location/111/48154')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});

describe('Deleting a location', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .post('/location/delete/')
            .send({
                "locationid":"111"
            })
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });
});