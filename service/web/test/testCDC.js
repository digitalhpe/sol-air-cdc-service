"use strict";

const should = require('should');
const assert = require('assert');
const request = require('supertest');
const app = require('../app');

describe('Summary data', function () {

    it('should return 200 for a valid call', function (done) {
        this.timeout(5000);
        request(app)
            .get('/cdc/4/Apache')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }

                res.should.have.property('status', 200);

                done();
            });
    });

    it('should return 400 for an valid state', function (done) {
        this.timeout(5000);
        request(app)
            .get('/cdc/75/Apache')
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.should.have.property('status', 400);

                done();
            });
    });
});