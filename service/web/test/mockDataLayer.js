"use strict";

const uuid = require('node-uuid');
const Location = require('../model/locationSchema');
const data = module.exports = {};

data.saveLocation = function(location, callback) {

    const locationid = uuid.v4();

    callback(null, locationid);
};

data.deleteLocation = function(locationid, callback) {

    callback(null, locationid);
};


data.getLocations = function(id, callback) {

    const locations = [];

    let loc = new Location({
        pushid: "111",
        name: "Location One",
        latitude: 42.6191200,
        longitude: -83.2946550,
        zip: "48341",
        favorite: true,
        locationid: "c959464d-0324-4dd2-8024-2b0292a6b3df",
        notify: true,
        os: "ios"
    });

    locations.push(loc);

    loc = new Location({
        pushid: "111",
        name: "Location Two",
        latitude: 34.103003,
        longitude: -118.410468,
        zip: "90210",
        favorite: false,
        locationid: "a49c6dc3-6929-4748-8c5c-308292f52b72",
        notify: false,
        os: "ios"
    });

    locations.push(loc);

    callback(null, locations);
};


data.findLocation = function(id, callback) {

    if(id === 0) {
        callback(null, undefined);
    } else {
        const location = {};
        location.zip = 90210;
        location.latitude = 34.103003;
        location.longitude = -118.410468;
        location.name = "Location Two";
        location.favorite = true;
        location.pushid = 111;
        location.locationid = "a49c6dc3-6929-4748-8c5c-308292f52b72";
        location.notify = true;
        location.os = "android";
        callback(null, undefined);
    }
};

data.getStates = function() {
    return new Promise(function (resolve) {
        const result = [];

        let state = {};
        state.code = "4";
        state.name = 'Arizona';

        result.push(state);

        resolve(result);
    });
};