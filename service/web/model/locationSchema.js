"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const locationSchema = new Schema({
    pushid: String,
    locationid: String,
    name: String,
    latitude: Number,
    longitude: Number,
    zip: String,
    favorite: Boolean,
    notify: Boolean,
    os: String,
    county: String,
    state: Number,
    flood: Number,
    park: Number,
    measures: [{
        year: Number,
        hospital: Number,
        er: Number
    }]

});

const Location = mongoose.model('locations', locationSchema);

// make this available to our users in our Node applications
module.exports = Location;