"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const stateSchema = new Schema({
    code: String,
    name: String,
    abbreviation: String
});

const State = mongoose.model('states', stateSchema);

// make this available to our users in our Node applications
module.exports = State;