"use strict";

module.exports = {
    logDir: "log-test",
    ssl: false,
    connectionString: "NotNeededForTesting",
    epaKey: "NotNeededForTesting",
    gcmKey: "NotNeededForTesting",
    cdcKey: "NotNeededForTesting"
};