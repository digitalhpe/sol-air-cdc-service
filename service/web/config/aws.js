
module.exports = {

    logDir: "/var/log/solair",
    ssl: false,
    connectionString: "mongo:27017/solavi",
    epaKey: "EPA_AIRNOW_KEY_HERE",
    gcmKey: "GCM_KEY_HERE",
    cdcKey: "CDC_KEY_HERE"
};
