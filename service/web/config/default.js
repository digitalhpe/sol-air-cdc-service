
module.exports = {

    logDir: "log-dev",
    ssl: false,
    connectionString: "localhost:27017/solavi",
    epaKey: "EPA_AIRNOW_KEY_HERE",
    gcmKey: "GCM_KEY_HERE",
    cdcKey: "CDC_KEY_HERE"
};