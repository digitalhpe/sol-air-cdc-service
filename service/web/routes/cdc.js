"use strict";

const express = require('express');
const router = express.Router();

router.get('/:state/:county', async function(req, res) {

    const utils = req.app.locals.utils;
    const service = req.app.locals.service;
    const dataLayer = req.app.locals.dataLayer;

    const state = req.params.state;
    const county = req.params.county;

    const result = {};
    result.result = "OK";
    result.message = "";

    const stateList = await dataLayer.getStates();

    if(stateList.length === 0) {
        result.result = "Error";
        result.message = "Invalid Database";
        res.status(500).json(result);
        return;
    }

    if (!validState(state, stateList)) {
        result.result = "Error";
        result.message = "Bad State Code";
        res.status(400).json(result);
    } else {
        service.getAllMeasures(state, county)
            .then(function(result){
                res.status(200).json(result);
            })
            .catch(function(error){
                utils.handleError(error, res);
            });
    }


    function validState(stateCode, data) {
        for (let i = 0; i < data.length; i++) {
            if (data[i].code === stateCode) {
                return true;
            }
        }
        return false;
    }
});

module.exports = router;