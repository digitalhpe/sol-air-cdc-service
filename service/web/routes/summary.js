"use strict";

const express = require('express');

const router = express.Router();

router.get('/:id', function(req, res) {

    const dataLayer = req.app.locals.dataLayer;
    const utils = req.app.locals.utils;
    const service = req.app.locals.service;

    const result = {};
    result.result = "OK";
    result.message = "";

    const input = req.params.id;
    if (input === undefined) {
        result.result = "Error";
        result.message = "Request ID Missing";
        res.status(400).json(result);
    }

    dataLayer.getLocations(input, function(err, records){
        if (err) {
            utils.handleError(err, res);
        } else {

            service.getLanding(records, function(err, data){
                if (err) {
                    utils.handleError(err, res);
                } else {
                    result.locations = data;
                    res.status(200).json(result);
                }
            });
        }
    });

});

module.exports = router;