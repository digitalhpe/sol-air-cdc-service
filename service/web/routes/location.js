"use strict";

const express = require('express');
const async = require('async');

const router = express.Router();
const Location = require('../model/locationSchema');
const parser = require('../data/parser');

router.post('/add', async function(req, res) {

    const dataLayer = req.app.locals.dataLayer;
    const utils = req.app.locals.utils;
    const service = req.app.locals.service;

    const result = {};
    result.result = "OK";
    result.message = "";

    const stateList = await dataLayer.getStates();

    if(stateList.length === 0) {
        result.result = "Error";
        result.message = "Invalid Database";
        res.status(500).json(result);
        return;
    }

    const state = req.body.state;
    const county = req.body.county;

    const input = req.body;

    if (input === undefined) {
        result.result = "Error";
        result.message = "Request Body Missing";
        res.status(400).json(result);
    }

    //Convert the input to a Mongoose Location
    const loc = new Location({
        pushid: input.pushid,
        name: input.name,
        latitude: input.latitude,
        longitude: input.longitude,
        zip: input.zip,
        favorite: input.favorite,
        locationid: input.locationid,
        notify: input.notify,
        os: input.os,
        county: input.county,
        state: input.state
    });

    if (!validState(state, stateList)) {
        result.result = "Error";
        result.message = "Bad State Code";
        res.status(400).json(result);
    } else {
        service.getAllMeasures(state, county)
            .then(function(data){
                loc.flood = data.flood;
                loc.park = data.park;
                loc.measures = data.measures;
                return updateFavorite(loc.favorite);
            })
            .then(
                function(){
                    return saveLocation(loc, result);
                }
            )
            .then(function(){
                res.status(200).json(result);
            })
            .catch(function(err){
                utils.handleError(err, res);
            });
    }

    function updateFavorite(isFavorite) {
        return new Promise(function (resolve, reject) {
            if(isFavorite) {
                dataLayer.getLocations(loc.pushid, function(err, records){
                    if (err) {
                        reject(err);
                    } else {
                        async.map(records, function(document, next){
                            document.favorite = false;
                            document.save(next);
                        }, function(err, results) {
                            if(err) {
                                reject(err);
                            } else {
                                resolve();
                            }
                        });
                    }
                });
            } else {
                resolve();
            }
        });
    }

    function saveLocation(loc, result) {
        return new Promise(function (resolve, reject) {
            dataLayer.saveLocation(loc, function (err, locationid) {
                if (err) {
                    reject(err);
                } else {
                    result.locationid = locationid;
                    result.park = loc.park;
                    result.flood = loc.flood;
                    result.measures = loc.measures;
                    resolve();
                }
            });
        });
    }

    function validState(stateCode, data) {
        for (let i = 0; i < data.length; i++) {
            if (data[i].code === stateCode) {
                return true;
            }
        }
        return false;
    }
});

router.get('/history/:zip/:date', function(req, res, next) {
    const utils = req.app.locals.utils;
    const service = req.app.locals.service;

    const zip = req.params.zip;
    const time = req.params.date;

    service.getHistory(zip, time, function(err, data) {
        if(err) {
            utils.handleError(err, res);
        } else {
            res.status(200).json(parseHistoryResults(data));
        }
    });

    function parseHistoryResults(data) {
        const result = {};

        result.aqi = parser.parseAqi(data.rawHistoryAqi);
        result.aqiplus = parser.parseAqi(data.rawHistoryPlus);
        result.aqiminus = parser.parseAqi(data.rawHistoryMinus);

        return result;
    }
});

router.get('/:id/:zip', function(req, res, next) {

    const dataLayer = req.app.locals.dataLayer;
    const utils = req.app.locals.utils;
    const service = req.app.locals.service;

    const result = {};
    result.result = "OK";
    result.message = "";

    const zip = req.params.zip;
    if (zip === undefined) {
        result.result = "Error";
        result.message = "Zip Missing";
        res.status(400).json(result);
    }

    const id = req.params.id;
    if (id === undefined) {
        result.result = "Error";
        result.message = "Id Missing";
        res.status(400).json(result);
    }

    dataLayer.findLocation(id, function(err, data) {

        if (err) {
            utils.handleError(err, res);
        } else {
            var location = {};
            if(data === undefined) {
                location.zip = zip;
                location.locationid = id;
            } else {
                location.zip = data.zip;
                location.latitude = data.latitude;
                location.longitude = data.longitude;
                location.name = data.name;
                location.favorite = data.favorite;
                location.pushid = data.pushid;
                location.locationid = data.locationid;
                location.notify = data.notify;
                location.os = data.os;
            }

            service.getDetail(location, function(err, data) {
                if(err) {
                    utils.handleError(err, res);
                } else {
                    res.status(200).json(parser.parseDetailResults(location,data));
                }
            });
        }
    });
});

router.post('/delete', function(req, res, next) {

    const dataLayer = req.app.locals.dataLayer;
    const utils = req.app.locals.utils;

    const result = {};
    result.result = "OK";
    result.message = "";

    const input = req.body;

    if (input === undefined) {
        result.result = "Error";
        result.message = "Request Body Missing";
        res.status(400).json(result);
    }

    if (input.locationid === undefined) {
        result.result = "Error";
        result.message = "locationid Missing";
        res.status(400).json(result);
    }

    dataLayer.deleteLocation(input.locationid, function (err, data) {
        if (err) {
            utils.handleError(err, res);
        } else {
            res.status(200).json(result);
        }
    });
});

module.exports = router;