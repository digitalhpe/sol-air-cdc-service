"use strict";

const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {
    const result = {};
    result.message = 'Bazinga!';
    res.status(200).json(result);
});

module.exports = router;