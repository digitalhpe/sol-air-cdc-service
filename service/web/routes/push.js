"use strict";

const express = require('express');

const pushService = require('../data/push');

const router = express.Router();


router.get('/', function(req, res) {

    const dataLayer = req.app.locals.dataLayer;
    const service = req.app.locals.service;

    pushService.sendNotifications(dataLayer, service);

    const result = {};
    result.result = "OK";
    result.message = "";

    res.status(200).json(result);

});

module.exports = router;