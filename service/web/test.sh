#!/bin/bash

echo
echo  "UNIT TEST:  *** Setting up environment ***"
echo 
npm install

echo
echo  "UNIT TEST:  *** Run Lint ***"
echo 
npm run inspect

echo
echo  "UNIT TEST:  *** Run Tests ***"
echo 
npm test
