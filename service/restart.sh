#!/bin/bash

echo
echo  "DOCKER:  *** Stopping containers ***"
echo 
docker-compose stop

echo 
echo "DOCKER:  *** Restarting Images and Containers ***"
echo 
docker-compose up -d
