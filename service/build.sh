#!/bin/bash

if [ -z ${BUILD_NUMBER+x} ];
	then 
		export BUILD_NUMBER=1
fi

echo
echo  "DOCKER:  *** Stopping containers ***"
echo 
docker-compose down

echo
echo "DOCKER:  *** Removing images ***"
docker rmi $(docker images | awk '$1 ~ /^solair/ { print $3 }')

echo
echo "DOCKER:  *** Creating and starting new containers ***"
docker-compose up -d

if [ "$BUILD_TYPE" = "Reset" ];
	then
		echo
		echo "MONGODB:  *** Resetting initial data ***"
		docker exec solair-db mongo /data/dbsetup.js
fi