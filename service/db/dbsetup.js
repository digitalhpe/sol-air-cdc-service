var conn = new Mongo();
var db = conn.getDB("solavi");

db.locations.drop();

db.locations.insert({"locationid" : "acbea724-1483-4869-b4e5-4ba472e0f609", "pushid" : "111", "name" : "Freedom Tower", "latitude" : 25.780071, "longitude" : -80.188991, "zip" : "33132", "favorite" : false, "notify": true, "os": "ios"});
db.locations.insert({"locationid" : "3a997d1d-bf94-48e9-be5f-9cccc543d763", "pushid" : "111", "name" : "Julia Ideson Building", "latitude" : 29.758978, "longitude" : -95.368466, "zip" : "77002", "favorite" : false, "notify": false, "os": "ios"});
db.locations.insert({"locationid" : "1506fb37-175b-4e3a-94d1-42ec820234f8", "pushid" : "111", "name" : "Manns Chinese Theater", "latitude" : 34.101655, "longitude" : -118.341224, "zip" : "90028", "favorite" : true, "notify": false, "os": "android"});
db.locations.insert({"locationid" : "bc2e375e-8754-49d0-a46a-70d1e9ea5ae6", "pushid" : "111", "name" : "Guardian Building", "latitude" : 42.329135, "longitude" : -83.045981, "zip" : "48226", "favorite" : false, "notify": true, "os": "android"});

db.states.drop();
db.states.insert({"code":"1", "name" :  "Alabama", "abbreviation" : "AL"});
db.states.insert({"code":"2", "name" :  "Alaska", "abbreviation" : "AK"});
db.states.insert({"code":"4", "name" :  "Arizona", "abbreviation" : "AZ"});
db.states.insert({"code":"5", "name" :  "Arkansas", "abbreviation" : "AR"});
db.states.insert({"code":"6", "name" :  "California", "abbreviation" : "CA"});
db.states.insert({"code":"8", "name" :  "Colorado", "abbreviation" : "CO"});
db.states.insert({"code":"9", "name" :  "Connecticut", "abbreviation" : "CT"});
db.states.insert({"code":"10", "name" :  "Delaware", "abbreviation" : "DE"});
db.states.insert({"code":"11", "name" :  "District of Columbia", "abbreviation" : "DC"});
db.states.insert({"code":"12", "name" :  "Florida", "abbreviation" : "FL"});
db.states.insert({"code":"13", "name" :  "Georgia", "abbreviation" : "GA"});
db.states.insert({"code":"15", "name" :  "Hawaii", "abbreviation" : "HI"});
db.states.insert({"code":"16", "name" :  "Idaho", "abbreviation" : "ID"});
db.states.insert({"code":"17", "name" :  "Illinois", "abbreviation" : "IL"});
db.states.insert({"code":"18", "name" :  "Indiana", "abbreviation" : "IN"});
db.states.insert({"code":"19", "name" :  "Iowa", "abbreviation" : "IA"});
db.states.insert({"code":"20", "name" :  "Kansas", "abbreviation" : "KS"});
db.states.insert({"code":"21", "name" :  "Kentucky", "abbreviation" : "KY"});
db.states.insert({"code":"22", "name" :  "Louisiana", "abbreviation" : "LA"});
db.states.insert({"code":"23", "name" :  "Maine", "abbreviation" : "ME"});
db.states.insert({"code":"24", "name" :  "Maryland", "abbreviation" : "MD"});
db.states.insert({"code":"25", "name" :  "Massachusetts", "abbreviation" : "MA"});
db.states.insert({"code":"26", "name" :  "Michigan", "abbreviation" : "MI"});
db.states.insert({"code":"27", "name" :  "Minnesota", "abbreviation" : "MN"});
db.states.insert({"code":"28", "name" :  "Mississippi", "abbreviation" : "MS"});
db.states.insert({"code":"29", "name" :  "Missouri", "abbreviation" : "MO"});
db.states.insert({"code":"30", "name" :  "Montana", "abbreviation" : "MT"});
db.states.insert({"code":"31", "name" :  "Nebraska", "abbreviation" : "NE"});
db.states.insert({"code":"32", "name" :  "Nevada", "abbreviation" : "NV"});
db.states.insert({"code":"33", "name" :  "New Hampshire", "abbreviation" : "NH"});
db.states.insert({"code":"34", "name" :  "New Jersey", "abbreviation" : "NJ"});
db.states.insert({"code":"35", "name" :  "New Mexico", "abbreviation" : "NM"});
db.states.insert({"code":"36", "name" :  "New York", "abbreviation" : "NY"});
db.states.insert({"code":"37", "name" :  "North Carolina", "abbreviation" : "NC"});
db.states.insert({"code":"38", "name" :  "North Dakota", "abbreviation" : "ND"});
db.states.insert({"code":"39", "name" :  "Ohio", "abbreviation" : "OH"});
db.states.insert({"code":"40", "name" :  "Oklahoma", "abbreviation" : "OK"});
db.states.insert({"code":"41", "name" :  "Oregon", "abbreviation" : "OR"});
db.states.insert({"code":"42", "name" :  "Pennsylvania", "abbreviation" : "PA"});
db.states.insert({"code":"44", "name" :  "Rhode Island", "abbreviation" : "RI"});
db.states.insert({"code":"45", "name" :  "South Carolina", "abbreviation" : "SC"});
db.states.insert({"code":"46", "name" :  "South Dakota", "abbreviation" : "SD"});
db.states.insert({"code":"47", "name" :  "Tennessee", "abbreviation" : "TN"});
db.states.insert({"code":"48", "name" :  "Texas", "abbreviation" : "TX"});
db.states.insert({"code":"49", "name" :  "Utah", "abbreviation" : "UT"});
db.states.insert({"code":"50", "name" :  "Vermont", "abbreviation" : "VT"});
db.states.insert({"code":"51", "name" :  "Virginia", "abbreviation" : "VA"});
db.states.insert({"code":"53", "name" :  "Washington", "abbreviation" : "WA"});
db.states.insert({"code":"54", "name" :  "West Virginia", "abbreviation" : "WV"});
db.states.insert({"code":"55", "name" :  "Wisconsin", "abbreviation" : "WI"});
db.states.insert({"code":"56", "name" :  "Wyoming", "abbreviation" : "WY"});